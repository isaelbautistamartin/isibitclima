# isibit_clima

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


Clonar el proyecto con el siguiente comando "git clone https://gitlab.com/isaelbautistamartin/isibitclima.git"

Una vez clonado se debe de mover el directorio con el siguiente comando "cd isibitclima"

Dentro del directorio para poser visualizarlo en manera local corra el siguiente comando, "npm run serve", asegurese que se crearon los archivos que se describe mas abajo para que pueda funcionar correctamente.


Se deben de crear dos archivos para las variables de configuración dentro de la carpeta raíz, con los
siguientes nombres ".env.development.local, .env.production.local", ahi se debe de escribir la siguiente 
variable de entorno (VUE_APP_OPEN_WEATHER_API_KEY=) pasandole la API_KEY.

En la siguiente linea anexo la API_KEY ya que esta solo fue creada para realizar pruebas.
VUE_APP_OPEN_WEATHER_API_KEY=fe7910a0b60a11b35a145bdd13654154

Este proyecto le configure el lint de AirbNb, y debido a que lo desarrolle en un equipo con windows 
al menos en mi equipo muestra un error de lint relacionado con "Expected linebreaks to be 'LF' but found 'CRLF'  linebreak-style", solamente es parar la ejecución y correr el siguiente comando "npm run lint -- --fix" y volver a compilar el proyecto en modo de desarrollo.