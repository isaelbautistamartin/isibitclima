import Api from '@/services/Api';

export default {
  getWeatherByCity(city) {
    return Api().get(`forecast?q=${city}&exclude=hourly&APPID=${process.env.VUE_APP_OPEN_WEATHER_API_KEY}`);
  },
};
